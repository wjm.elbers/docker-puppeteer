// puppeteer-extra is a drop-in replacement for puppeteer,
// it augments the installed puppeteer with plugin functionality
const puppeteer = require('puppeteer-extra');
const fs = require('fs');
const handler = require('/puppeteer/handler.js');

// add stealth plugin and use defaults (all evasion techniques)
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
puppeteer.use(StealthPlugin());

(async () => {
    console.log('Setting up scraper...');
    let url = "https://www.example.com/"
    if(process.env.SCRAPE_URL) {
        url = process.env.SCRAPE_URL;
    }
    console.log('   url:', url);

    let output = "none";
    if(process.env.SCRAPE_OUTPUT) {
        output = process.env.SCRAPE_OUTPUT;
    }
    console.log('   output:', output);

    let output_dir = "/data";
    if(process.env.SCRAPE_OUTPUT_DIR) {
        output_dir = process.env.SCRAPE_OUTPUT_DIR;
    }

    let output_file = "";
    if(process.env.SCRAPE_OUTPUT_FILE) {
        output = "file";
        output_file = process.env.SCRAPE_OUTPUT_FILE;
    }

    const browser = await puppeteer.launch({
        headless: true,
        args: [
            "--disable-gpu",
            "--disable-dev-shm-usage",
            "--disable-setuid-sandbox",
            "--no-sandbox",
            ]
    });

    const page = await browser.newPage();

    console.log('Starting scraper...');
    const result = await fetchAndProcessPage(page, url, handler.handle);

    //Process output
    switch(output) {
        case "stdout": writeResultToStdOut(result); break;
        case "file": writeResultToFile(output_dir, output_file, result); break;
        default: writeResultToStdOut(result);
    }

    //close down
    await page.close();
    await browser.close();
})();


const writeResultToFile = (dir, file, result) => {
    let dest = dir+file;
    if(dir.endsWith("/") && file.startsWith("/")) {
        dest = dir.substring(0, dir.length-1)+file;
    } else if(!dir.endsWith("/") && !file.startsWith("/")) {
        dest = dir + "/" + file;
    }

    fs.writeFileSync(dest, JSON.stringify(result));
    console.log("Result written to: "+dest);
}

const writeResultToStdOut = (result) => {
    console.log(result);
}

async function fetchAndProcessPage(page, url, handler) {
    const result = await page.goto(url);
    console.log(url + ": HTTP "+result.status());
    if (result.status() === 404) {
        return null;
    }
    await page.waitForTimeout(1000); //TODO: improve waiting, example: https://stackoverflow.com/a/61304202
    return await handler(page, new URL(url));
}