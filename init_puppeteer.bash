#!/usr/bin/env bash

mkdir -p tmp
(cd tmp && npm install puppeteer puppeteer-extra puppeteer-extra-plugin-stealth && cp package* ../image)
rm -r tmp