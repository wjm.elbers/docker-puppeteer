// puppeteer-extra is a drop-in replacement for puppeteer,
// it augments the installed puppeteer with plugin functionality
const puppeteer = require('puppeteer-extra');
const fs = require('fs');

// add stealth plugin and use defaults (all evasion techniques)
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
puppeteer.use(StealthPlugin());

(async () => {
    console.log('Setting up scrape...');

    const browser = await puppeteer.launch({
        headless: true,
        args: [
            "--disable-gpu",
            "--disable-dev-shm-usage",
            "--disable-setuid-sandbox",
            "--no-sandbox",
        ]
    });

    const page = await browser.newPage();

    console.log('Starting scrape...');
    const result = await fetchAndProcessPage(page, "https://www.af.nl/voorraad/massief/", fetchHoutsoorten);

    const flat = [];
    result.forEach(r => {
      r.data.forEach(d => {
         flat.push({
             href: r.href,
             titles: r.titles,
             t: d.t,
             l: d.l,
             b: d.b,
             d: d.d,
             p: d.p
         });
      });
    });

    //write result
    const output_dir = "/output/";
    const output_file = "houtlijst-af.json";
    fs.writeFileSync(output_dir+output_file, JSON.stringify(result));
    fs.writeFileSync(output_dir+"houtlijst-af-flat.json", JSON.stringify(flat));

    //close down
    await page.close();
    await browser.close();
})();

async function fetchAndProcessPage(page, url, handler) {
    const result = await page.goto(url);
    console.log(url + ": HTTP "+result.status());
    if (result.status() === 404) {
        return null;
    }
    await page.waitForTimeout(1000); //TODO: improve waiting, example: https://stackoverflow.com/a/61304202
    return await handler(page);
}

async function fetchHoutsoorten(page) {
     //const divCount = await page.$$eval('.style7 > center:nth-child(1) > div:nth-child(2) > center:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2) > p:nth-child(7)', divs => divs.length);
    const els = await page.$x('/html/body/div/center/div/center/table/tbody/tr[1]/td[2]/p[2]');

    const result = [];
    for(let i = 0; i <els.length; i++) {
        const data = await els[i].$$eval('a', links => {
            return links.map(link => {
                let href = link.href;
                if (!href.endsWith("/")) {
                    href += "/";
                }
                return {
                    href: href,
                    href_prijs: href+"prijslijst.htm",
                    title: link.textContent.trim(),
                    data: []

                };
            });
        });

        data.forEach(d => {
            let idx = -1;
            for(let i = 0; i < result.length && idx < 0; i++) {
                if(result[i].href === d.href) {
                    idx = i;
                }
            }

            if(idx < 0) {
                result.push({
                    href: d.href,
                    href_prijs: d.href_prijs,
                    titles: [d.title],
                    data: []

                });
            } else {
                result[idx].titles.push(d.title);
            }
        });
    }

    for(let i = 0; i < result.length; i++) {
        if(result[i].href_prijs) {
            let prijs_result = await fetchAndProcessPage(page, result[i].href_prijs, fetchPrijslijst);
            if(prijs_result === null) {
                result[i].href_prijs = result[i].href_prijs + "l";
                prijs_result = await fetchAndProcessPage(page, result[i].href_prijs, fetchPrijslijst);
            }
            if(prijs_result != null) {
                result[i].data = prijs_result;
            }
        }
    }

    return result;
}

async function fetchPrijslijst(page) {
    const tableRows = await page.$x('//tr');
    let header = [];
    let data = [];
    for(let i = 0; i < tableRows.length; i++) {
        const columns = await tableRows[i].$x('td');
        if(columns.length === 5) {
            if(header.length <= 0) {//header row
                for(let j = 0; j < columns.length; j++) {
                   const txt = await page.evaluate(col => col.innerText, columns[j]);
                   header.push(txt.trim());
                }
            } else { //data row
                const row = [];
                for(let j = 0; j < columns.length; j++) {
                   const txt = await page.evaluate(col => col.innerText, columns[j]);
                   const value = txt.trim();
                   //if(value !== "") {
                       row.push(value);
                   //}
                }

                let empty = true;
                for(let j = 0; j < row.length; j++) {
                    if(row[j] !== '') {
                        empty = false;
                    }
                }

                if(!empty) { //ignore empty rows
                    data.push(row);
                }
            }
        }
    }

    let idx_l = 1;
    let idx_b = 2;
    let idx_d = 3;
    let idx_p = 4;
    for(let i = 0; i < header.length; i++) {
        const hdr = header[i].toLowerCase();
        if(hdr.includes("lengte")) {
            idx_l = i;
        } else if(hdr.includes("breedte")) {
            idx_b = i;
        } else if(hdr.includes("dikte")) {
            idx_d = i;
        } else if(hdr.includes("prijs")) {
            idx_p = i;
        }
    }

    const getPriceValue = (val) => {
        if(val) {
            if (val.endsWith('/m3')) {
                val = val.replaceAll('/m3', '');
            } else if (val.endsWith('m3')) {
                val = val.replaceAll('m3', '');
            }

            val = val.replaceAll('/', '');
            val = val.replaceAll('.', '');
            val = val.trim();
        }
         return val;
    }

    const priceFilter = (p) => {
        if(!p) {
            return false;
        }
        if(p.includes("btw")) {
            return false;
        }
        if(p.includes("prijs")) {
            return false;
        }
        return true;
    }

    const result = [];
    let type = '';
    data.forEach(d => {
        if(d[0] !== "") {
            type = d[0];
        }

        let l = d[idx_l];
        let b = d[idx_b];
        let _d = d[idx_d];
        let p = d[idx_p];
        if(l.includes("\n")) {
            let l_arr = l.split("\n");
            let b_arr = b.split("\n");
            let d_arr = _d.split("\n");
            let p_arr = p.split("\n");
            for(var i = 0; i < l_arr.length; i++) {
                const price = getPriceValue(p_arr[i]);
                if(priceFilter(price)) {
                    result.push({
                        t: type,
                        l: l_arr[i],
                        b: b_arr[i],
                        d: d_arr[i],
                        p: price,
                    });
                }
            }
        } else {
            const price = getPriceValue(d[idx_p]);
            if(priceFilter(price)) {
                result.push({
                    t: type,
                    l: d[idx_l],
                    b: d[idx_b],
                    d: d[idx_d],
                    p: price
                });
            }
        }
    });

    return result;
}
