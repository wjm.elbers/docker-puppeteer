// puppeteer-extra is a drop-in replacement for puppeteer,
// it augments the installed puppeteer with plugin functionality
const puppeteer = require('puppeteer-extra');
const fs = require('fs');

// add stealth plugin and use defaults (all evasion techniques)
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
puppeteer.use(StealthPlugin());

(async () => {
    console.log('Running scrape...');

    const browser = await puppeteer.launch({
        headless: true,
        args: [
            "--disable-gpu",
            "--disable-dev-shm-usage",
            "--disable-setuid-sandbox",
            "--no-sandbox",
        ]
    });

    const page = await browser.newPage();

    await page.goto("https://www.funda.nl");
    await page.waitForTimeout(1000); //TODO: improve waiting, example: https://stackoverflow.com/a/61304202

    //Handle cookies
    const [button] = await page.$x('//button[contains(., "Accepteer alle cookies")]');
    if(button) {
        button.click();
        console.log("Clicked accept cookies button");
        await page.waitForNavigation();
    } else {
        console.log("No cookie button found");
    }

    const timestamp = new Date();
    const term = "Nijmegen";
    const result = await runSearch(page, term);
    const output = {ts: timestamp.toISOString(), term: term, result: result.results, processing: result.processing };
    const output_dir = "/output/";
    const output_file = "funda_scrape_"+term+"_"+Math.round(timestamp.getTime()/1000)+".json";
    fs.writeFileSync(output_dir+output_file, JSON.stringify(output));

    await page.close();
    await browser.close();

    console.log('All done, results in '+output_dir+' written to: '+output_file+'. ✨');
})();

function cleanupText(text) {
    if(!text) {
        return "";
    }
    return text.replaceAll("\n", " ");
}

async function getFirstNodeText(page, cssSelector) {
    const result = await page.$$eval(cssSelector, els => els.length > 0 ? els[0].innerText : null );
    return cleanupText(result);
}

async function getLastNodeText(page, cssSelector) {
    const result = await page.$$eval(cssSelector, els => els.length > 0 ? els[0].innerText : null );
    return cleanupText(result);
}

/**
 * Submit a search term and process all result pages
 *
 * @param page
 * @param term
 * @returns {Promise<[]>}
 */
async function runSearch(page, term) {
    let result = { results: [], processing: []};

    let pageIndex = 1;
    //Search for the specified term
    const searchBox = await page.$eval('#autocomplete-input', (el, t) => el.value = t, term);
    //await page.screenshot({path: "/output/screenshot_searchbox.png"});
    const [btnSearch] = await page.$x('//button[contains(., "Zoek")]');
    if(btnSearch) {
        try {
            //Run the search and wait for the page to load
            console.log("Waiting to process page " + pageIndex);
            btnSearch.click();
            const t1 = Date.now();
            await page.waitForNavigation();
            const delta = Date.now()-t1;

            //Get initial number of pages
            const totalResults = await getFirstNodeText(page, 'h1.search-output-result-count')
            const lastPage = await getLastNodeText(page, 'div.pagination-pages > a');
            console.log("Results: " + totalResults + ", last page = " + lastPage);

            //Process the first page of results
            const data = await extractResults(page, pageIndex);
            data.processing.load_time = delta;
            result.results = result.results.concat(data.results);
            result.processing = result.processing.concat(data.processing);

            //Keep clicking the next button while it is available and process the results for each page.
            let btnNext = await getNextButton(page);
            while (btnNext && btnNext != null) {
                pageIndex++;
                console.log("Waiting to process page " + pageIndex);
                //Click the button and wait for the page to load
                btnNext.click();
                const t1 = Date.now();
                await page.waitForNavigation();
                const delta = Date.now()-t1;

                //Process the page of results
                const data = await extractResults(page, pageIndex);
                data.processing.load_time = delta;
                result.results = result.results.concat(data.results);
                result.processing = result.processing.concat(data.processing);

                //Select the next button
                btnNext = await getNextButton(page);

                await page.waitForTimeout(5000); //Rate limit to 12 requests / minute
            }
        } catch(e) {
            console.log("Search failed. Error:");
            console.log(e);
            await page.screenshot({path: "/output/screenshot_search_error.png"});
        }
    } else {
        console.log("No search button found");
        await page.screenshot({path: "/output/screenshot_no_search_button.png"});
    }

    return result;
}

async function getNextButton(page) {
    let next = null;
    try {
        const [btn] = await page.$x('//a[@rel="next"]');
        next = btn;
    } catch (e) {
        console.log("Failed to extract next button: "+e);
    }
    return next;
}

function parseAddress(value) {
    const cleaned = cleanupText(value);
    let addressMatch = cleaned.match(/(.+) (\d+) (\d\d\d\d .+) (.+)/); //Example: Waalhaven 1 6541 AG Nijmegen
    if(addressMatch == null) {
        addressMatch = cleaned.match(/(.+) (\d+ .+) (\d\d\d\d .+) (.+)/); //Example: Waalhaven 1 e 6541 AG Nijmegen
    }
    if(addressMatch == null) {
        addressMatch = cleaned.match(/(.+) \((.+)\) (\d\d\d\d .+) (.+)/); //Example: Hart van de Waalsprong Donk en Elft (Bouwnr. 1) 6663 RA Lent
    }
    if(addressMatch == null) {
        addressMatch = cleaned.match(/(.+) \((.+)\) (\d\d\d\d) (.+)/); //Example: //C1 (Bouwnr. 16) 6663 Lent
    }

    if(addressMatch == null) {
        console.log("Failed to match: ["+cleaned+"]")
        return null;
    }

    const street = addressMatch[1];
    const number = addressMatch[2];
    const zipcode = addressMatch[3];
    const city = addressMatch[4];
    return {street: street, number: number, zipcode: zipcode, city: city};
}

function parsePrice(value) {
    const priceMatch = value.match(/€ (.+) k\.k\./);
    return priceMatch != null ? priceMatch[1] : 0;
}

function parseProps(value) {
    if(value === "") {
        return null;
    }

    let properties = null;
    const propsMatch0 = value.match(/(\d+) m²/);
    const propsMatch1 = value.match(/(\d+) m²\s(\d+) kamer[s]?/);
    const propsMatch2 = value.match(/(\d+) m² \/ (\d+) m²\s(\d+) kamer[s]?/);
    if (propsMatch2 != null) {
        properties = {size: propsMatch2[1], size_total: propsMatch2[2], rooms: propsMatch2[3]};
    } else if (propsMatch1 != null) {
        properties = {size: propsMatch1[1], size_total: 0, rooms: propsMatch1[2]};
    } else if (propsMatch0 != null) {
        properties = {size: propsMatch0[1], size_total: 0, rooms: 0};
    } else {
        console.log("Failed to match properties: [" + value + "]");
    }
    return properties;
}

/**
 * Extract results from a paged funda search page
 * @param page
 * @param pageIndex The index of the current search page
 * @returns {Promise<[]>}
 */
async function extractResults(page, pageIndex) {
    const t1 = Date.now();
    //await page.screenshot({path: "/output/screenshot_page_"+pageIndex+".png"});

    const data = await page.$$eval('.search-result-main', results => {
        const data = [];
        results.forEach(result => {
            const address = result.querySelector('div.search-result__header-title-col').innerText;
            const price = result.querySelector('span.search-result-price').innerText;
            const props = result.querySelector('ul.search-result-kenmerken').innerText;
            const link = result.querySelector('a[data-object-url-tracking="resultlist"]').getAttribute("href");
            const el = result.querySelector('ul.labels');
            const html = result.innerHTML;

            let labels = "";
            if(el) {
                labels = el.innerText;
            }

            data.push({address: address, price: price, properties: props, link: link, labels: labels, html: html});
        });
        return data;
    });

    const result = [];
    data.forEach(d => {
        result.push({
            address: parseAddress(d.address),
            price: parsePrice(d.price),
            properties: parseProps(d.properties),
            link: d.link,
            labels: d.labels,
            html: null//d.html
        });
    });

    const delta = Date.now()-t1;
    return {results: result, processing: {index: pageIndex, processing_time: delta}};
}