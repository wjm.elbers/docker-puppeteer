// puppeteer-extra is a drop-in replacement for puppeteer,
// it augments the installed puppeteer with plugin functionality
const puppeteer = require('puppeteer-extra');
const fs = require('fs');

// add stealth plugin and use defaults (all evasion techniques)
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
puppeteer.use(StealthPlugin());

(async () => {
    console.log('Setting up scraper...');

    const browser = await puppeteer.launch({
        headless: true,
        args: [
            "--disable-gpu",
            "--disable-dev-shm-usage",
            "--disable-setuid-sandbox",
            "--no-sandbox",
        ]
    });

    const page = await browser.newPage();

    console.log('Starting scrape...');
    //https://www.gamma.nl/assortiment/l/hout/houten-platen
    const result = await fetchAndProcessPage(page, "https://www.gamma.nl/assortiment/l/hout/balken-latten-planken", processFirstPage);

    console.log("Result:");
    result.forEach(r => {
        console.log("R: "+r.info+" = "+r.price);
    });
/*
    const flat = [];
    result.forEach(r => {
      r.data.forEach(d => {
         flat.push({
             href: r.href,
             titles: r.titles,
             t: d.t,
             l: d.l,
             b: d.b,
             d: d.d,
             p: d.p
         });
      });
    });
*/

    /*
    //write result
    const output_dir = "/output/";
    const output_file = "houtlijst-af.json";
    fs.writeFileSync(output_dir+output_file, JSON.stringify(result));
    fs.writeFileSync(output_dir+"houtlijst-af-flat.json", JSON.stringify(flat));
*/
    //close down
    await page.close();
    await browser.close();
})();

async function fetchAndProcessPage(page, url, handler) {
    const result = await page.goto(url);
    console.log(url + ": HTTP "+result.status());
    if (result.status() === 404) {
        return null;
    }
    await page.waitForTimeout(1000); //TODO: improve waiting, example: https://stackoverflow.com/a/61304202
    return await handler(page);
}

async function getInnerText(el) {
    if(!el) {
        return '';
    }
    const contentHandle = await el.getProperty('innerText');
    const contentValue = await contentHandle.jsonValue();
    return contentValue;
}

async function getAttributeText(el) {
    //const [link] = await els[i].$x('.//a/@title');
    const linkHandle = await el.getProperty('value');
    const propertyValue = await linkHandle.jsonValue();
    return propertyValue;
}

async function processFirstPage(page) {
    const idx = 1;
    let results = await extractResultsFromPage(page);
    console.log("Extracted "+(results ? results.length : 0)+" result(s) from page with idx="+idx);
    if(results) {
        //Process next page and merge results
        const pageResults = await processNextPage(page, idx+1);
        if(pageResults) {
            results.concat(pageResults);
        }
    }
    return results;
}

async function processNextPage(page, idx) {
    let results = [];

    const [elPagination] = await page.$x('//div[@class="pagination"]');
    const elPaginationLinks = await elPagination.$x('.//a');
    for(let i = 0; i < elPaginationLinks.length; i++) {
        const elPaginationLink = elPaginationLinks[i];
        const linkText = await getInnerText(elPaginationLink);

        if(linkText === "❯") {
            //Click the link to move to the next page and wait for the next page to load
            elPaginationLink.click();
            await page.waitForNavigation();

            //Process the new page and merge the results
            results = await extractResultsFromPage(page);
            console.log("Extracted "+(results ? results.length : 0)+" result(s) from page with idx="+idx);

            //Rate limit to 12 requests / minute
            await page.waitForTimeout(5000);
        }
    };

    console.log("Results before:", results);

    //Try to navigate to the next page
    if(results.length > 0) {
        const pageResults = await processNextPage(page, idx+1);
        if(pageResults) {
            results.concat(pageResults);
        }
    }

    console.log("Results after:", results);

    return results;
}

async function extractResultsFromPage(page) {
    const els = await page.$x('//article');

    const result = [];
    for(let i = 0; i <els.length; i++) {
        const [content] = await els[i].$x('.//div[@class="product-tile-content"]');

        const [info] = await content.$x('.//div[@class="product-tile-name  js-product-name"]');
        const [price] = await content.$x('.//div[@class="product-tile-price"]');
        const infoValue = await getInnerText(info);
        let priceVal = await getInnerText(price);
        if(priceVal !== "") {
            priceVal = priceVal.replaceAll('\n', '');
        }

        result.push({
            info: infoValue,
            price: priceVal,
        });
    }

    return result;
}
/**
 * docker run -ti --rm -v "$(pwd)/${dir_scripts}:/scripts" docker-puppeteer:$(git describe --always) --no-deprecation gamma.js
 */

