// puppeteer-extra is a drop-in replacement for puppeteer,
// it augments the installed puppeteer with plugin functionality
const puppeteer = require('puppeteer-extra');
const fs = require('fs');

// add stealth plugin and use defaults (all evasion techniques)
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
puppeteer.use(StealthPlugin());

(async () => {
    console.log('Running scraper...');

    const browser = await puppeteer.launch({
        headless: true,
        args: [
            "--disable-gpu",
            "--disable-dev-shm-usage",
            "--disable-setuid-sandbox",
            "--no-sandbox",
        ]
    });

    const page = await browser.newPage();
    await page.setViewport({width: 3840,height: 2160,deviceScaleFactor: 1,});

    const base_url = "https://www.globalfirepower.com";
    const t1 = new Date().getTime();
    const countries = await fetch(page, base_url+"/countries.php", fetchCountries);
    const elapsed1 = new Date().getTime() - t1;
    console.log("Fetched "+countries.length+" countries ("+elapsed1+" ms).");

    const output_dir = "/output/";
    const t2 = new Date().getTime();
    for(var i = 0; i < countries.length; i++) {
        const country = countries[i];
        const country_data = await fetch(page, base_url+country.href, fetchCountryDetails);
        const elapsed2 = new Date().getTime() - t2;

        const result = { id: country.id, title: country.title, data: country_data};
        const filename = output_dir+"country-military-strength-detail-"+country.id+".json";
        fs.writeFileSync(filename, JSON.stringify(result));
        console.log("Written: "+filename+" (progress="+i+"/"+countries.length+")");
    }
    const elapsed3 = new Date().getTime() - t2;
    console.log("Finished in "+elapsed3+" ms.");

    await page.close();
    await browser.close();
})();

async function fetch(page, url, handler) {
    await page.goto(url);
    await page.waitForTimeout(1000);

     //Handle cookies
    const [button] = await page.$x('//button[contains(., "Do not consent")]');
    if(button) {
        button.click();
        console.log("Clicked do not consent cookies button");
        await page.waitForTimeout(1000);
    }/* else {
        console.debug("No cookie button found");
    }*/

    //await page.screenshot({path: "/output/screenshot_countries.png"});
    return await handler(page);
}

async function fetchCountries(page) {
    const cssSelector = "a.picTrans";
    const result = await page.$$eval(cssSelector, results => {
        const data = [];
        results.forEach(result => {
            const rawCode = result.innerText.replace("\n", "");
            const rawHref = result.getAttribute("href");
            const rawTitle = result.getAttribute("title");

            let year = "0";
            let title = "";
            let id = "";

            const arrHref = rawHref.match(/.*country_id=(.+)/)
            if ( arrHref !== null ) {
                id = arrHref[1];
            }

            const arr = rawTitle.match(/The current military power of the nation of (.+) for the year (.+)/)
            if( arr !== null ) {
                title = arr[1];
                year = arr[2];
            }

            if(rawCode !== "") {
                data.push({
                    code: rawCode,
                    href: rawHref,
                    title: title,
                    year: year,
                    id: id
                });
            }
        });
        return data;
    });

    return result;
}

async function fetchCountryDetails(page) {
    const cssSelector = "div.specsGenContainers";
    const result = await page.$$eval(cssSelector, results => {

        /**
         * Return a a normalized value
         *
         * @param value
         * @returns {{unit: *, value: string}|{unit: null, value: *}}
         */
        function normalizeValue(value) {
            const m1 = value.match(/(.+) \((.+)%\)/);
            if (m1 !== null) {
                return {
                    value: m1[1].replaceAll(",", ""),
                    unit: null
                }
            }

            const m2 = value.match(/(.+) (.+)/);
            if(m2 !== null) {
                return {
                    value: m2[1].replaceAll(",", "").replaceAll("$", ""),
                    unit: m2[2]
                }
            }

            return {
                value: value.replaceAll(",", ""),
                unit: null
            }
        }

        const data = [];
        results.forEach(result => {
            const text = result.innerText
            if(text !== "") {
                const parts = text.split('\n');
                if(parts.length === 3) {
                    const key = parts[1].replaceAll(":", "");
                    const result = normalizeValue(parts[2]);
                    data.push({key: key, value: result.value, unit: result.unit});
                }
            }
        });
        return data;
    });

    return result;
}
